# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 13:16:25 2017

@author: IKHAN
"""

import numpy as np
import xlrd
import csv
from itertools import cycle
from pylab import *
import os 
import sys

from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4.QtGui import QColor

from ui_MainWindow import Ui_MplMainWindow
from ui_ResourceDialogue import Ui_PlotSettingDialog


##This class deal with the settings dialogue box##
###############################################################################
class ResourceDialogue(QtGui.QDialog, Ui_PlotSettingDialog):
    
    ########################
    def __init__(self,cwd_start,parent = None):
        super(ResourceDialogue, self).__init__(parent)
        self.setupUi(self)
        self.show()
        self.cwd_start = cwd_start
        filename = self.cwd_start+'\settings.data'
        if os.path.isfile(filename):
            with open(filename) as f:
                lines = f.readlines()
                lines = [x.strip() for x in lines]
        
        self.lineEdit_axisFont.setText(lines[0])
        self.lineEdit_labelFont.setText(lines[1])
        self.lineEdit_titleFont.setText(lines[2])
        self.lineEdit_lineColor.setText(lines[3])
        self.lineEdit_lineStyle.setText(lines[4])
        self.lineEdit_lineSize.setText(lines[5])
        self.checkBox_GridMaj.setChecked(int(lines[6]))
        self.checkBox_GridMin.setChecked(int(lines[7]))
        
        self.lineEdit_lineColor.setToolTip('Specify line colors\n\nExample\nr,g,b,k,c,m,y,w\nAuto (default)')
        self.lineEdit_lineStyle.setToolTip('Specify line types\n\nExample\n-,:,--,-.')
        self.lineEdit_lineSize.setToolTip('Specify line widths\n\nExample\n2,1.5,2,3')
                
        self.pushButton_setDefault.clicked.connect(self.func_setDefault)
        self.pushButton_setSave.clicked.connect(self.func_setSave)
        self.pushButton_addColor.clicked.connect(self.func_addColor2List)
        
        
    
    ########################    
    def func_addColor2List(self):
        color = QtGui.QColorDialog.getColor()
        if color.name():
            if self.lineEdit_lineColor.text():
                self.lineEdit_lineColor.setText(self.lineEdit_lineColor.text()+','+color.name())
            else:
                self.lineEdit_lineColor.setText(color.name())
        
    ########################
    def func_setDefault(self):
        self.lineEdit_axisFont.setText('15')
        self.lineEdit_labelFont.setText('12')
        self.lineEdit_titleFont.setText('15')
        self.lineEdit_lineColor.setText('Auto')
        self.lineEdit_lineStyle.setText('-')
        self.lineEdit_lineSize.setText('2')
        self.checkBox_GridMaj.setChecked(1)
        self.checkBox_GridMin.setChecked(0)
        self.func_setSave()
    
    ########################
    def func_setSave(self):
        var1  = self.lineEdit_axisFont.text()
        var2  = self.lineEdit_labelFont.text()
        var3  = self.lineEdit_titleFont.text()
        var4  = self.lineEdit_lineColor.text()
        var5  = self.lineEdit_lineStyle.text()
        var6  = self.lineEdit_lineSize.text()
        var7  = int(self.checkBox_GridMaj.isChecked())
        var8  = int(self.checkBox_GridMin.isChecked())
        
        
        filename = self.cwd_start+'\settings.data'
        with open(filename, 'w') as f:
            f.write(var1 + '\n' )
            f.write(var2 + '\n' )
            f.write(var3 + '\n' )
            f.write(var4 + '\n' )
            f.write(var5 + '\n' )
            f.write(var6 + '\n' )
            f.write(str(var7) + '\n' )
            f.write(str(var8))
            f.close()
        self.close()
        
###############################################################################
class DesignerMainWindow(QtGui.QMainWindow, Ui_MplMainWindow):
    
    ########################
    def __init__(self,  cwd_start, parent = None):
        super(DesignerMainWindow, self).__init__(parent)
        self.setupUi(self)
        self.cwd_start = cwd_start
        
        self.pushButton_Set.clicked.connect(self.func_Set)
        self.pushButton_Clear.clicked.connect(self.func_Clear)
        self.pushButton_BgColor.clicked.connect(self.func_BgColor)
        self.actionLoad.triggered.connect(self.func_Load)
        self.actionSet_working_directory.triggered.connect(self.func_Set_working_dir)
        self.actionExport_settings.triggered.connect(self.func_ExportSettings)
        self.actionImport_settings.triggered.connect(self.func_ImportSettings)
        self.actionAbout.triggered.connect(self.func_About)
        
        self.actionSettings.triggered.connect(self.open_Settings)
        self.checkBox_Freeze.clicked.connect(self.func_FreezeCheck)
        
        self.lineEdit_Xlabel.setText('X Label')
        self.lineEdit_Ylabel.setText('Y Label')
        self.lineEdit_Title.setText('Title')
        self.lineEdit_Scale.setText('1.0')
        self.func_getAxisLims()
        
        filename = cwd_start+'\working_dir.data'
        if os.path.isfile(filename):
            with open(filename) as f:
                self.dir_name = f.readline()
        else:
            self.dir_name = cwd_start
 
    
    
    def func_Set_working_dir(self):
        self.dir_name = QtGui.QFileDialog.getExistingDirectory(self,"Select Working Directory")
        filename = self.cwd_start+'\working_dir.data'
        with open(filename, 'w') as f:
            f.write(self.dir_name)
        
    ########################    
    def func_Set(self):
        try:
            self.data
        except:
            self.data = []
        if len(self.data):
            self.func_readSettingFile()
            XLab  = self.lineEdit_Xlabel.text()
            YLab  = self.lineEdit_Ylabel.text()
            Title = self.lineEdit_Title.text()
            self.Scale = float(self.lineEdit_Scale.text())
            
            
            self.mpl.canvas.ax.clear()
            
            self.mpl.canvas.ax.set_xlabel(XLab, fontsize = self.lines[0])
            self.mpl.canvas.ax.set_ylabel(YLab, fontsize = self.lines[0])
            self.mpl.canvas.ax.set_title(Title, fontsize = self.lines[2])
            
            self.xmin  = self.lineEdit_Xmin.text()
            self.xmax  = self.lineEdit_Xmax.text()
            self.ymin  = self.lineEdit_Ymin.text()
            self.ymax  = self.lineEdit_Ymax.text()
            
            self.func_Draw()
        else:
            print('No data...')
    
    ########################    
    def func_Draw(self):

        p=0
        n=int(np.size(self.data,1)/2.0)
        k=0
        l=0
        w=0
        for n in range(n):
            linesize = [x.strip() for x in self.lines[5].split(',')]
            if w < len(linesize):
                linewidth = linesize[w]
            else:
                w = 0
                linewidth = linesize[w]
            
            linetype = [x.strip() for x in self.lines[4].split(',')]
            if l < len(linetype):
                linestyle = linetype[l]
            else:
                l = 0
                linestyle = linetype[l]
            
            if self.lines[3]=='Auto':
                self.mpl.canvas.ax.plot(self.data[:,p],self.data[:,p+1]*self.Scale,label=self.LabelD[p+1],linestyle=linestyle,linewidth=float(linewidth))
            else:
                color_list = [x.strip() for x in self.lines[3].split(',')]
                if k < len(color_list):
                    col = color_list[k]
                    k + 1
                else:
                    k = 0
                    col = color_list[k]
                self.mpl.canvas.ax.plot(self.data[:,p],self.data[:,p+1]*self.Scale,label=self.LabelD[p+1],color=col,linestyle=linestyle,linewidth=float(linewidth))

            k+=1    
            p+=2
            l+=1
            w+=1

        
        legend = self.mpl.canvas.ax.legend(prop={'size': self.lines[1]})
        legend.draggable()
        
        self.mpl.canvas.ax.set_xlim([float(self.xmin), float(self.xmax)])
        self.mpl.canvas.ax.set_ylim([float(self.ymin), float(self.ymax)])
        
        if int(self.lines[6]):
            self.mpl.canvas.ax.grid(which='major', linestyle='-', linewidth='0.5')
            
        if int(self.lines[7]):
            self.mpl.canvas.ax.minorticks_on()
            self.mpl.canvas.ax.grid(which='minor', linestyle=':', linewidth='0.5')
        
        
        self.mpl.canvas.draw()
    
    ########################
    def func_Load(self):
        self.func_readSettingFile()
        
        file = QtGui.QFileDialog.getOpenFileName(self, ("Load File"), self.dir_name, ("Data Files (*.xlsx *.xls *.csv)"))
        filename, file_extension = os.path.splitext(file)
        
        if file:
            if file_extension == '.csv':
                file=open(file,'r')
                reader=csv.reader(file,delimiter=',')
                self.LabelD = next(reader) #read labels
                ncols = len(self.LabelD)   #find number of columns
                nrows = len(list(reader))  #find number of data rows starting from line 2
                file.seek(0)               #goback to the begining of the file
                next(reader)               #jump to next line 
                p = 0
                self.data = np.zeros((nrows,ncols))
                for r in reader:
                    for c in range(ncols):
                        cellval = r[c]
                        if not cellval:
                            cellval = None
                        self.data[p,c] = cellval
                    p = p+1
            
            if file_extension == '.xls' or file_extension == '.xlsx':
                xl_workbook = xlrd.open_workbook(file)
                xl_sheet = xl_workbook.sheet_by_index(0)
                nrows = xl_sheet.nrows
                ncols = xl_sheet.ncols
                self.LabelD = []
                for i in range(ncols):
                    self.LabelD.append(xl_sheet.cell(0,i).value)
                p =0
                self.data = np.zeros((nrows-1,ncols))
                for r in range(1, nrows):
                    for c in range(ncols):
                        cellval = xl_sheet.cell_value(r,c)
                        if cellval=='':
                            cellval = None
                        self.data[p,c]=cellval
                    p+=1
            
            XLab  = self.lineEdit_Xlabel.text()
            YLab  = self.lineEdit_Ylabel.text()
            Title = self.lineEdit_Title.text()
            Scale = float(self.lineEdit_Scale.text())
            
            self.mpl.canvas.ax.clear()
    
            p=0
            n=int(np.size(self.data,1)/2.0)
            k=0
            l=0
            w=0
            for n in range(n):
                linesize = [x.strip() for x in self.lines[5].split(',')]
                if w < len(linesize):
                    linewidth = linesize[w]
                else:
                    w = 0
                    linewidth = linesize[w]
                
                linetype = [x.strip() for x in self.lines[4].split(',')]
                if l < len(linetype):
                    linestyle = linetype[l]
                else:
                    l = 0
                    linestyle = linetype[l]
                
                if self.lines[3]=='Auto':
                    self.mpl.canvas.ax.plot(self.data[:,p],self.data[:,p+1]*Scale,label=self.LabelD[p+1],linestyle=linestyle,linewidth=float(linewidth))
                else:
                    color_list = [x.strip() for x in self.lines[3].split(',')]
                    if k < len(color_list):
                        col = color_list[k]
                        k + 1
                    else:
                        k = 0
                        col = color_list[k]
                    self.mpl.canvas.ax.plot(self.data[:,p],self.data[:,p+1]*Scale,label=self.LabelD[p+1],color=col,linestyle=linestyle,linewidth=float(linewidth))
    
                k+=1    
                p+=2
                l+=1
                w+=1
    
    
            legend = self.mpl.canvas.ax.legend(prop={'size': self.lines[1]})
            legend.draggable()
            self.func_getAxisLims()
            
            self.mpl.canvas.ax.set_xlabel(XLab, fontsize = self.lines[0])
            self.mpl.canvas.ax.set_ylabel(YLab, fontsize = self.lines[0])
            self.mpl.canvas.ax.set_title(Title, fontsize = self.lines[2])
            
            if int(self.lines[6]):
                self.mpl.canvas.ax.grid(which='major', linestyle='-', linewidth='0.5')
                
            if int(self.lines[7]):
                self.mpl.canvas.ax.minorticks_on()
                self.mpl.canvas.ax.grid(which='minor', linestyle=':', linewidth='0.5')
            
            xmin  = self.lineEdit_Xmin.text()
            xmax  = self.lineEdit_Xmax.text()
            ymin  = self.lineEdit_Ymin.text()
            ymax  = self.lineEdit_Ymax.text()
            
            self.mpl.canvas.ax.set_xlim([float(xmin), float(xmax)])
            self.mpl.canvas.ax.set_ylim([float(ymin), float(ymax)])
            self.mpl.canvas.draw()
    
    ########################
    def func_getAxisLims(self):
        if not self.checkBox_Freeze.isChecked():
            self.lineEdit_Xmin.setText(str(self.mpl.canvas.ax.get_xlim()[0]))
            self.lineEdit_Xmax.setText(str(self.mpl.canvas.ax.get_xlim()[1]))
            self.lineEdit_Ymin.setText(str(self.mpl.canvas.ax.get_ylim()[0]))
            self.lineEdit_Ymax.setText(str(self.mpl.canvas.ax.get_ylim()[1]))

    
    ########################
    def func_BgColor(self):
        self.color = QtGui.QColorDialog.getColor()
        if self.color.name():
            self.mpl.canvas.ax.set_facecolor(self.color.name())
            self.mpl.canvas.draw()
    
    ########################
    def open_Settings(self):
        self.dialogue = self.StartDiag()
    
    ########################
    def StartDiag(self):
        dialogue = ResourceDialogue(self.cwd_start)
        dialogue.show()
        return dialogue
    
    ########################
    def func_readSettingFile(self):
        filename = self.cwd_start+'\settings.data'
        if os.path.isfile(filename):
            with open(filename) as f:
                self.lines = f.readlines()
                self.lines = [x.strip() for x in self.lines]
                f.close()
    
    ########################
    def func_FreezeCheck(self):
        if self.checkBox_Freeze.isChecked():
            self.lineEdit_Xmin.setDisabled(True)
            self.lineEdit_Xmax.setDisabled(True)
            self.lineEdit_Ymin.setDisabled(True)
            self.lineEdit_Ymax.setDisabled(True)
            self.lineEdit_Xlabel.setDisabled(True)
            self.lineEdit_Ylabel.setDisabled(True)
            self.lineEdit_Title.setDisabled(True)
            self.lineEdit_Scale.setDisabled(True)
        else:
            self.lineEdit_Xmin.setDisabled(False)
            self.lineEdit_Xmax.setDisabled(False)
            self.lineEdit_Ymin.setDisabled(False)
            self.lineEdit_Ymax.setDisabled(False)
            self.lineEdit_Xlabel.setDisabled(False)
            self.lineEdit_Ylabel.setDisabled(False)
            self.lineEdit_Title.setDisabled(False)
            self.lineEdit_Scale.setDisabled(False)
    
    ########################
    def func_ExportSettings(self):
        filename = self.cwd_start+'\settings.data'
        if os.path.isfile(filename):
            with open(filename) as f:
                lines = f.readlines()
                lines = [x.strip() for x in lines]
            f.close()
        
            lines.append(self.lineEdit_Xmin.text())
            lines.append(self.lineEdit_Xmax.text())
            lines.append(self.lineEdit_Ymin.text())
            lines.append(self.lineEdit_Ymax.text())
            lines.append(self.lineEdit_Xlabel.text())
            lines.append(self.lineEdit_Ylabel.text())
            lines.append(self.lineEdit_Title.text())
            lines.append(self.lineEdit_Scale.text())
        
        
        filename = QtGui.QFileDialog.getSaveFileName(self, ("Save File"), self.dir_name, ("Data File (*.data)"))
        with open(filename, 'w') as f:
            f.write('#AIRPLOTALLSETTINGSFILE#' + '\n' )
            for line in lines:
                f.write(line + '\n' )
            f.close()
    
    ########################
    def func_ImportSettings(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, ("Load File"), self.dir_name, ("Data File (*.data)"))
        if filename:
            with open(filename) as f:
                lines = f.readlines()
                lines = [x.strip() for x in lines]
            if lines[0] == '#AIRPLOTALLSETTINGSFILE#':
                plotfile = cwd_start+'\settings.data'
                with open(plotfile, 'w') as f:
                    for i in range(1,9):
                        f.write(lines[i] + '\n' )
                    self.lineEdit_Xmin.setText(lines[9])
                    self.lineEdit_Xmax.setText(lines[10])
                    self.lineEdit_Ymin.setText(lines[11])
                    self.lineEdit_Ymax.setText(lines[12])
                    self.lineEdit_Xlabel.setText(lines[13])
                    self.lineEdit_Ylabel.setText(lines[14])
                    self.lineEdit_Title.setText(lines[15])
                    self.lineEdit_Scale.setText(lines[16])
                    f.close()
            else:
                print('Incorrect format..')
    
    ########################
    def func_Clear(self):
        self.checkBox_Freeze.setChecked(False)
        self.func_FreezeCheck()
        self.mpl.canvas.ax.clear()
        self.mpl.canvas.ax.set_facecolor('White')
        self.mpl.canvas.ax.set_xlim([0, 1])
        self.mpl.canvas.ax.set_ylim([0, 1])
        self.lineEdit_Xmin.setText('0.0')
        self.lineEdit_Xmax.setText('1.0')
        self.lineEdit_Ymin.setText('0.0')
        self.lineEdit_Ymax.setText('1.0')
        self.lineEdit_Xlabel.setText('X Label')
        self.lineEdit_Ylabel.setText('Y Label')
        self.lineEdit_Title.setText('Title')
        self.lineEdit_Scale.setText('1.0')
        self.LabelD = []
        self.data = []
        self.mpl.canvas.draw()                
    
    ########################
    def func_About(self):
        QtGui.QMessageBox.about(self, "About","Imad M. Khan\nimad.mahfooz@gmail.com")
        
    
###############################################################################
if __name__ == '__main__':
    
    ### Check for the setting fil####
    cwd_start = os.getcwd()

    filename = cwd_start+'\settings.data'
    if not os.path.isfile(filename):
        print('creating default setting file...')
        with open(filename, 'w') as f:
            f.write('20' + '\n' )
            f.write('20' + '\n' )
            f.write('20' + '\n' )
            f.write('Auto' + '\n' )
            f.write('-' + '\n' )
            f.write('2' + '\n' )
            f.write('1' + '\n' )
            f.write('1')
            f.close()
            
  
    app = QtGui.QApplication(sys.argv)
    dmw = DesignerMainWindow(cwd_start)
    dmw.show()
    sys.exit(app.exec_())
        